package com.example.demo.ui

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.example.demo.databinding.SumFragmentBinding

class SumFragment:Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = SumFragmentBinding.inflate(inflater, container, false)
        val builder = context?.let { AlertDialog.Builder(it) }
        binding.num1
        binding.num2
        binding.calculate.setOnClickListener {
            if (binding.num1.text.isEmpty()) {
                Toast.makeText(context, "First field should not be empty ", Toast.LENGTH_SHORT).show()
            } else if (binding.num2.text.isEmpty()) {
                Toast.makeText(context, "Second field should not be empty ", Toast.LENGTH_SHORT).show()
            } else {
                builder?.setTitle("Sum dialog")
                val sum = (binding.num1.text.toString().toInt() + binding.num2.text.toString().toInt()).toString()
                builder?.setMessage("Your calculation result is:"+ sum)
                builder?.setNegativeButton("Cancel"){ dialogInterface: DialogInterface, id: Int ->
                    dialogInterface.dismiss()
                }
                builder?.show()
            }
         binding.clear.setOnClickListener {
                binding.num1.setText("")
                binding.num2.setText("")
                binding.viewResult.setText("")
            }
        };return binding.root
        }
    }

package com.example.demo.ui
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController

import com.example.demo.databinding.MainFragmentBinding
import com.example.demo.ui.MainFragmentDirections.Companion.actionMainFragmentToSumFragment

class MainFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = MainFragmentBinding.inflate(inflater, container, false)
        binding.button.setOnClickListener {
        findNavController().navigate(MainFragmentDirections.actionMainFragmentToSumFragment())
        }
        return binding.root
    }

}